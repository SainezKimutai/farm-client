# farm-client 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Assumptions

. A farm with reported locust outbreak has 80% loss impact on its yield

. A farm on the wind direction of a farm with locust outbreak has 50% loss impact on its yield

. A farm that is neighboring a locust outbreak farm has 25% oss impact on its yield

. A farm with reported locust outbreak and is within another farm that has been reported locust outbreak has 105 % (80 + 25) loss impact on its yield

. A farm with reported locust outbreak and is on the wind direction of another farm that has been reported locust outbreak has 130 % (80 + 50) loss impact on its yield

. A farm thats is on the wind direction of a farm that has been reported locust outbreak and is neighboring another farm with locust outbreak has 75 % (50 + 25) loss impact on its yield

. A farm thats is neighboring two farms with locust outbreak has 50 % (25 + 25) loss impact on its yield