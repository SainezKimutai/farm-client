export default {
    methods: {
        getData(url) {
            return new Promise((resolve, reject) => {
                this.$http.get(url).then((response) => {
                    response.status === 200 ? resolve(response) : reject({error_message: response.statusText})
                }).catch((err) => { reject({error_message: err.statusText})})
            })
        }
    }
}