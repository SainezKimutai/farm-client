
import Home from './components/home/home.vue';
import NotFound from './components/notFound/notFound.vue'
import Report from './components/report/report.vue';

export default [
    {path: '/', redirect: '/home', pathMatch: 'full' },
    {path: '/home', name: 'Home', component: Home},
    {path: '/report', name: 'Report', component: Report, props: true,
    beforeEnter: (to, from, next) => { // if props is not passed, redirect to homepage
        to.params.farmBodyResponse ? next() : next({ name: 'Home' })
      }},
    {path: '**', name: 'Report', component: NotFound}
]
