import Vue from 'vue'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import Routes from './mainRouting';

Vue.config.productionTip = false

// Http 
Vue.use(VueResource);

// Register Routes
Vue.use(VueRouter);

export const Eventbus = new Vue(); 

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
