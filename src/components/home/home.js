import dataMixin from '../../mixins/dataMixin'

export default {
    name: 'Home',
    data () {
        return {
            inputUrl: '',
            inputEmpty: false,
            SpinnerLaunch: false
        }
    },
    methods: {
        submitUrl() {
            this.inputUrl == '' ? this.inputEmpty = true : this.fetchData(this.inputUrl)
        },
        checkInputValidity() {
            this.inputUrl == '' ? this.inputEmpty = true : this.inputEmpty = false
        },
        fetchData(url) {
            this.SpinnerLaunch = true
            this.getData(url)
                .then((response) => {      
                    this.$router.push({ name: 'Report', params: {farmBodyResponse: response.body }})
                }).catch((err) => { console.error(err); this.SpinnerLaunch = false })
        },
    },
    mixins: [dataMixin],
    destroyed() {
        this.SpinnerLaunch = false
    },

}
