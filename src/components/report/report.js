
export default {
    name: 'Report',
    props: {
        farmBodyResponse: {
            type: Object,
            required: true 
        }
    },
    data () {
        return {
            farmData: {
                grid: this.farmBodyResponse.grid_width_and_length,
                gridArray: this.generateGridArray(this.farmBodyResponse.grid_width_and_length),
                wind: this.farmBodyResponse.wind_direction,
                outbreaks: this.farmBodyResponse.confirmed_outbreaks,
                mediumRiskFarms: [],
                lowRiskFarms: [],
                overalImpact: 0
            }
        }
    },
    methods: {
        generateGridArray(num) {
            return Array.from(new Array(num + 1), (x, i) => i)
        },
        checkHighRisk(x, y) {
            let isOfHighRisk = false;
            for (let index in this.farmData.outbreaks) {
                if (this.farmData.outbreaks[index].x == x && this.farmData.outbreaks[index].y == y) { isOfHighRisk = true; }
            }
            return isOfHighRisk;
        },
        checkMediumRisk(x, y) {
            let isOfMediumRisk = false;
            for (let index in this.farmData.mediumRiskFarms) {
                if (this.farmData.mediumRiskFarms[index].x == x && this.farmData.mediumRiskFarms[index].y == y) { isOfMediumRisk = true; }
            }
            return isOfMediumRisk;
        },
        checkLowRisk(x, y) {
            let isOfLowRisk = false;
            for (let index in this.farmData.lowRiskFarms) {
                if (this.farmData.lowRiskFarms[index].x == x && this.farmData.lowRiskFarms[index].y == y) { isOfLowRisk = true; }
            }
            return isOfLowRisk;
        },
        checkAllRiskValue(x, y) {
            let riskValue = 0;
            for (let index in this.farmData.outbreaks) {
                if (this.farmData.outbreaks[index].x == x && this.farmData.outbreaks[index].y == y) { riskValue = riskValue + 80 }
            }
            for (let index in this.farmData.mediumRiskFarms) {
                if (this.farmData.mediumRiskFarms[index].x == x && this.farmData.mediumRiskFarms[index].y == y) { riskValue = riskValue + 50 }
            }
            for (let index in this.farmData.lowRiskFarms) {
                if (this.farmData.lowRiskFarms[index].x == x && this.farmData.lowRiskFarms[index].y == y) { riskValue = riskValue + 25 }
            }
            return riskValue;
        },
        getMediumRiskFarm() {
            switch (this.farmData.wind.toLowerCase()) {
                case 'north': this.calculateMediumRiskFarms({x: 0, y: -1}); break; // move 1 negative of y-axis
                case 'east': this.calculateMediumRiskFarms({x: 1, y: 0}); break; // move 1 positive of x-axis 
                case 'south': this.calculateMediumRiskFarms({x: 0, y: 1}); break; // move 1 positive of y-axis 
                case 'west': this.calculateMediumRiskFarms({x: -1, y: 0}); break; // move 1 negative of x-axis  
                default: break
            }
        },
        
        getLowRiskFarm() {
            let north_area = {x: 0, y: -1}
            let east_area = {x: 1, y: 0}
            let south_area = {x: 0, y: 1}
            let west_area = {x: -1, y: 0}
            switch (this.farmData.wind.toLowerCase()) {
                case 'north': this.calculateLowRiskFarms(east_area, south_area, west_area); break;
                case 'south': this.calculateLowRiskFarms(west_area, north_area, east_area ); break;
                case 'east': this.calculateLowRiskFarms(south_area, west_area, north_area ); break; 
                case 'west': this.calculateLowRiskFarms(north_area, east_area, south_area ); break; 
                default: break
            }
        },


        calculateMediumRiskFarms(area_param) {
            this.farmData.outbreaks.forEach( area => {
                let new_area =  {x : area.x + area_param.x, y : area.y + area_param.y,}
                if (this.farmData.gridArray.indexOf(new_area.x) !== -1 && this.farmData.gridArray.indexOf(new_area.y) !== -1 ) {
                    this.farmData.mediumRiskFarms = [...this.farmData.mediumRiskFarms, new_area ]
                }
            });
        },

        calculateLowRiskFarms(area_1_param, area_2_param, area_3_param) {
            this.farmData.outbreaks.forEach( area => {
                let new_area_1 =  {x : area.x + area_1_param.x, y : area.y + area_1_param.y,}
                let new_area_2 =  {x : area.x + area_2_param.x, y : area.y + area_2_param.y,}
                let new_area_3 =  {x : area.x + area_3_param.x, y : area.y + area_3_param.y,}
                if (this.farmData.gridArray.indexOf(new_area_1.x) !== -1 && this.farmData.gridArray.indexOf(new_area_1.y) !== -1 ) {
                    this.farmData.lowRiskFarms = [...this.farmData.lowRiskFarms, new_area_1 ] 
                }
                if (this.farmData.gridArray.indexOf(new_area_2.x) !== -1 && this.farmData.gridArray.indexOf(new_area_2.y) !== -1 ) {
                    this.farmData.lowRiskFarms = [...this.farmData.lowRiskFarms, new_area_2 ]
                }
                if (this.farmData.gridArray.indexOf(new_area_3.x) !== -1 && this.farmData.gridArray.indexOf(new_area_3.y) !== -1 ) {
                    this.farmData.lowRiskFarms = [...this.farmData.lowRiskFarms, new_area_3 ]
                }
            });
        }
    },
    created() {
        this.getMediumRiskFarm();
        this.getLowRiskFarm();
    },
    computed: {
        getOverralRisk () {
            // high_risk_area should be within the grid 
           let high_risk_area = this.farmData.outbreaks.filter((area) => 
                this.farmData.gridArray.indexOf(area.x) !== -1 && this.farmData.gridArray.indexOf(area.y) !== -1 ).map(e => e).length
           let medium_risk_area = this.farmData.mediumRiskFarms.length
           let low_risk_area = this.farmData.lowRiskFarms.length
           let total_farms = (this.farmData.grid + 1) * (this.farmData.grid + 1) 

           return (((high_risk_area * 80) + (medium_risk_area * 50) + (low_risk_area * 25)) / total_farms).toFixed(2)

        }
    },

}
